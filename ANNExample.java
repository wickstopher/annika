/** 
 * An interface for examples that can be learned by a NeuralNetwork.
 * @author Christopher R. Wicks (cw9887@cs.rit.edu)
 */
public interface ANNExample {

    /** 
     * Represents the input values for this example.
     * @return An array containing the input values for this example.
     */
    public double[] getInput();

    /**
     * Represents the actual (expected) output for this example.
     * @return  The output of this example.
     */
    public double[] getOutput();
}