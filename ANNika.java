import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;
import java.io.UnsupportedEncodingException;

/**
 * Front-end class for training and querying NeuralNetwork instances on
 * LanguageExamples to the end of predicting whether a given input String is
 * in English, Italian or Dutch.
 * 
 * The main program has 3 modes of operation: Train indefinitely, train for
 * a specified number of epochs and then initiate query mode, or initiate
 * query mode from a serialized NeuralNetwork instance from the filesystem.
 * 
 * @author Christopher R. Wicks (cw9887@cs.rit.edu)
 */
public class ANNika {

    private static final String OUTFILE = "output.txt";

    private static int hSize, bestIteration, epochs, nTesting, nTraining;
    private static double tPercent, lowestTestError;
    private static long seed;

    private static ArrayList<LanguageExample> testingSet, trainingSet;
    private static NeuralNetwork annika;
    private static Random prng;

    /** Arguments:
     * 
     * If only 1 argument, args[0] is the filepath to a serialized 
     * NeuralNetwork object.
     * 
     * Otherwise:
     * args[0] = size of hidden layer (int)
     * args[1] = percentage of data to use as training set
     * args[2] = random seed (default is random)
     * args[3] = number of epochs to train (default is Integer.MAX_VALUE)
     *
     * If 1 argument is present, the program will attempt to read a serialized
     * NeuralNetwork instance from the specified filepath, and will then 
     * initiate a prompt at which the network can be queried.
     * 
     * If 2 arguments are present, the program will train indefinitely on a
     * randomized seed value, and when interrupted, append output to a file
     * called output.txt.
     *
     * If 3 arguments are present, the program will train indefinitely on the
     * provided seed value, and when interrupted, append output to a file
     * called output.txt.
     *
     * If 4 arguments are present, the program will train on the provided seed
     * value for the specified number of epochs, and will then initiate
     * a prompt at which the network can be queried for its interpretation of
     * arbitrary input. It will no longer learn, it will simply accept input
     * and generate output. The user can also choose to serialize the
     * NeuralNetwork instance to the filesystem from the prompt if so desired.
     */
    public static void main(String[] args) {

        try {

            //If only one argument, try to read a serialized NeuralNetwork
            //from the filesystem (and exit when user is done querying)
            if (args.length == 1) {
                NeuralNetwork annika = loadNetwork(args[0]);
                initiatePrompt(annika);
                System.exit(0);
            }

            //train endlessly if no epoch number has been specified
            boolean endless = args.length <= 3;

            //the 3rd argument is the seed
            boolean seeded = args.length >= 3;

            //size of hidden layer, specified on command-line
            hSize = Integer.parseInt(args[0]);
            
            //percentage of overall examples to be used for training, specified
            //on the command-line
            tPercent = 
                (double) Integer.parseInt(args[1]) / 100.0;

            //if we're running endlessly, add a shutdown hook to handle ctrl-c
            if (endless)
                Runtime.getRuntime().addShutdownHook(new ShutdownThread());

            //if no seed is provided, simply generate a random seed; otherwise
            //use the seed provided at the command-line.
            if (endless && !seeded)
                 seed = new Random().nextLong();
            else
                seed = Long.parseLong(args[2]);

            //set up a prng with the seed to pass to Collections.shuffle
            prng = new Random(seed);

            //if we're training, loop as long as possible, otherwise do the
            //number of epochs specified at the command-line
            if (endless)
                epochs = Integer.MAX_VALUE;
            else
                epochs = Integer.parseInt(args[3]);

            //Populate example sets (training and testing set)
            populateExampleSets();

            //Store the sizes of the testing and training sets
            nTraining = trainingSet.size();
            nTesting = testingSet.size();
            
            //instantiate the neural net
            annika = new NeuralNetwork(LanguageExample.INPUT_DIM, hSize, 
                LanguageExample.NUM_CLASSES);

            //set the weights with the random seed
            annika.setWeights(seed);

            //train for the specified number of epochs
            train();

            //If we aren't simply training endlessly, initiate the query prompt
            if (!endless)
                initiatePrompt(annika);
        }
        //Exception handling
        catch (NumberFormatException e) {
            System.err.println("Bad command-line argument.");
        }
        catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Specify hidden-layer argument.");
        }
        catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
        catch (IOException e) {
            System.err.println(e.getMessage());
        }
        catch (ClassNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Initiate a query prompt for the given NeuralNetwork instance. The user
     * will be able to submit query strings and serialize the NeuralNetwork
     * to the filesystem if so desired.
     * @param network   The NeuralNetwork instance that is to be queried.
     */
    public static void initiatePrompt(NeuralNetwork network) {

        //command Strings
        String QUIT = "q";
        String END = "d";
        String SAVE = "save";
        String TEST = "test";

        //silly random messages
        String[] msgs = { 
            "If I had to guess, I'd say that was %s.",
            "Hmm...that's tough. Was it %s?",
            "I think that was...%s? Are you trying to trick me?",
            "%s!!!",
            "Ok, yeah, that was %s."
        };

        //prng to pick silly random messages
        Random r = new Random();

        Scanner scanner = new Scanner(System.in);
        StringBuilder sb;
        String line = "";
        double[] guess;

        System.out.println
        ("\n|=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=|");
        System.out.println
        ("|                   Welcome to ANNika!                      |");
        System.out.println
        ("|  I just might be able to tell what you're talking about.  |");
        System.out.println
        ("|=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=|\n");
        System.out.println
            ("* To enter input, simply type (or paste) it into the terminal");
        System.out.printf
            ("  window followed by the \"%s\" command on a new line.\n", END);
        System.out.printf
            ("* To see this ANN's performance given all test data, type \"%s\".\n", TEST);
        System.out.printf
            ("* To save this ANN instance to the filesystem, type \"%s [filepath]\"\n", SAVE);
        System.out.printf("* Enter the \"%s\" command to quit.\n\n", QUIT);

        while (true) {

            System.out.printf
                ("Please enter your input:\n");

            line = scanner.nextLine();
            
            if (line.equals(QUIT)) break;
            if (line.equals(END)) continue;
            if (line.equals(TEST)) {
                testAllExamples(network);
                continue;
            }
            if (line.length() > SAVE.length() && line.substring(0, SAVE.length()).equals(SAVE)) {
                try {
                    String fileName = 
                        line.substring(SAVE.length() + 1, line.length());
                    writeNetwork(network, fileName);
                    System.out.println
                        ("\nNetwork successfully saved to " + fileName + "\n");
                }
                catch (IOException e) {
                    System.out.println(e.getMessage());
                }
                continue;
            }

            sb = new StringBuilder();

            while (!line.equals(END)) {
                sb.append(line);
                line = scanner.nextLine();
            }
            try {
                guess = network.getPrediction
                    (LanguageExample.parseLanguageString(sb.toString()));

                Language lang = Language.interpretResults(guess);
                System.out.print("\nANNika: ");
                System.out.printf(msgs[r.nextInt(msgs.length)], lang);
                System.out.print("\n\n");
            }
            catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Load a serialized NeuralNetwork instance from the filesystem.
     * @param path  The path to the file containing the serialized instance.
     * @return  The reconstituted NeuralNetwork instance.
     * @throws ClassNotFoundException if there is a problem reconstituting.
     * @throws FileNotFoundException if there is a problem opening the file.
     */
    public static NeuralNetwork loadNetwork(String path) 
    throws ClassNotFoundException, IOException {        
        ObjectInputStream instream = 
            new ObjectInputStream(new FileInputStream(path));

        Object o = instream.readObject();
        instream.close();
        return (NeuralNetwork) o;
    }

    /**
     * Write a NeuralNetwork instance out to the filesystem using serialization.
     * @param net   The NeuralNetwork to be saved.
     * @param path  The path at which to save the instance.
     * @throws IOException if there is a problem writing to the path.
     */
    public static void writeNetwork(NeuralNetwork net, String path)
    throws IOException {       
        ObjectOutputStream outstream = 
            new ObjectOutputStream(new FileOutputStream(path));

        outstream.writeObject((Object) net);
        outstream.close();
    }
    
    /**
     * Train the neural network on the specified input parameters.
     */
    private static void train() {
        
        double trainingError, testingError, error;
        double[] input, guess, output;

        //We want to store our lowest testing error and the iteration at
        //which it occurred so that we can reproduce the NeuralNetwork
        //if desired after testing.
        lowestTestError = Integer.MAX_VALUE;
        bestIteration = 0;

        for (int i = 1; i <= epochs; i++) {

            trainingError = testingError = 0;

            //backpropagate training examples
            for (LanguageExample e : trainingSet) {
                trainingError += annika.backpropagate(e);
            }

            //print the cumulative and scaled training errors for the epoch
            System.out.printf
            ("Iteration %d Training Error: %f (scaled value is %f)\n", 
              i, trainingError, (trainingError / (double) nTraining));

            //test the network with our testing set
            for (LanguageExample e : testingSet) {
                
                input = e.getInput();
                output = e.getOutput();
                guess = annika.getPrediction(input);

                //calculate the test error for this example
                for (int j = 0; j < output.length; j++) {
                    error = output[j] - guess[j];
                    testingError += error * error;
                }
            }

            //Print the cumulative and scaled test errors for the epoch
            System.out.printf
            ("Iteration %d Testing Error: %f (scaled value is %f)\n",
              i, testingError, (testingError / (double) nTesting));

            //If this is our best run so far, store the error and epoch
            if (testingError < lowestTestError) {
                lowestTestError = testingError;
                bestIteration = i;
            }
        }
    }

    /**
     * Instantiate and populate static variables testingSet and trainingSet.
     */
    private static void populateExampleSets() 
    throws FileNotFoundException, UnsupportedEncodingException {
        
        //parse language examples out of files
        ArrayList<LanguageExample> englishExamples = 
            LanguageExample.getExamples
            ("examples/english.txt", Language.ENGLISH);            

        ArrayList<LanguageExample> italianExamples = 
            LanguageExample.getExamples
            ("examples/italian.txt", Language.ITALIAN);

        ArrayList<LanguageExample> dutchExamples = 
            LanguageExample.getExamples
            ("examples/dutch.txt", Language.DUTCH);

        //instantiate training and testing sets
        trainingSet = new ArrayList<LanguageExample>();
        testingSet = new ArrayList<LanguageExample>();

        //Shuffle the example sets
        Collections.shuffle(englishExamples, prng);
        Collections.shuffle(italianExamples, prng);
        Collections.shuffle(dutchExamples, prng);

        //calculate number of training examples to use for each language
        int numEnglish = (int) (tPercent * englishExamples.size());
        int numItalian = (int) (tPercent * italianExamples.size());
        int numDutch = (int) (tPercent * dutchExamples.size());

        //Add the specified percentage of examples to the training set
        for (int i = 0; i < numEnglish; i++)
            trainingSet.add(englishExamples.remove(0));

        for (int i = 0; i < numItalian; i++)
            trainingSet.add(italianExamples.remove(0));

        for (int i = 0; i < numDutch; i++)
            trainingSet.add(dutchExamples.remove(0));

        //Add the rest of the examples to the testing set
        testingSet.addAll(englishExamples);
        testingSet.addAll(italianExamples);            
        testingSet.addAll(dutchExamples);

        //shuffle the input sets once more
        Collections.shuffle(trainingSet, prng);
        Collections.shuffle(testingSet, prng);
    }

    /**
     * Given a NeuralNetwork, test all training/testing data and print the
     * results to standard out.
     * @param network A NeuralNetwork instance.
     */
    private static void testAllExamples(NeuralNetwork network) {
        
        try {

            ArrayList<LanguageExample> examples = new ArrayList<LanguageExample>();

            //add all examples from files to examples list
            examples.addAll(LanguageExample.getExamples
                ("examples/english.txt", Language.ENGLISH));
            examples.addAll(LanguageExample.getExamples
                ("examples/italian.txt", Language.ITALIAN));
            examples.addAll(LanguageExample.getExamples
                ("examples/dutch.txt", Language.DUTCH));

            int wrong = 0;
            double[] guess;
            Language lang;

            //tally the number of incorrect classifications
            for (LanguageExample e : examples) {
                guess = network.getPrediction(e.getInput());
                lang = Language.interpretResults(guess);
                if (lang != e.getLanguage()) 
                    wrong++;
            }

            //
            System.out.println("\nTotal number of examples wrong: " + wrong);
            System.out.println("Total number of examples tested: " + 
                examples.size());
            System.out.printf("Percentage of examples wrong: %.2f",
                (100.0 * (double) wrong) / (double) examples.size());
            System.out.println("%\n");
        }
        catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
        catch (UnsupportedEncodingException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * A Thread class that is to be run at shutdown if so specified.
     * Appends an output file with data regarding the particular training run.
     * @author Christopher R. Wicks (cw9887@cs.rit.edu)
     */
    static class ShutdownThread extends Thread {
        public void run() {

            try {
                File outFile = new File(OUTFILE);
                PrintWriter outStream = 
                    new PrintWriter(new FileOutputStream(outFile, true));

                outStream.println("Seed used: " + seed);
                outStream.println("Size of hidden layer: " + hSize);
                outStream.println
                    ("Proportion of examples used for training: "+ tPercent);
                outStream.printf
                    ("Lowest testing error was %f, occured at iteration %d\n",
                    lowestTestError, bestIteration);
                outStream.printf
                    ("Scaled value of lowest testing error: %f\n",
                    (lowestTestError / (double) nTesting));
                outStream.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");

                outStream.close();
            }
            catch (Exception e) {}
        }
    }
}