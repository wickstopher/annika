FLAGS =     -encoding UTF-8
COMPILER =  javac
APP =       ANNika
BEST_ANN =  048277.ann

compile:
	$(COMPILER) $(FLAGS) *.java

clean:
	rm *.class

run:
	java $(APP) $(BEST_ANN)