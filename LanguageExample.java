import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

/**
 * An ANNExample class for languages. Represents either English, Dutch or
 * Italian language strings. Also contains relevant methods for parsing and
 * producing LanguageExamples.
 * @author Christopher R. Wicks (cw9887@cs.rit.edu)
 */
public class LanguageExample implements ANNExample {

    /**
     * A separator to use in a file that contains language examples.
     */
    public final static String SEPARATOR = "$$$";
    
    /**
     * The input dimension of a LanguageExample.
     */
    public final static int INPUT_DIM = 9;
    
    /**
     * The number of classes (possible languages) that can are represented.
     */
    public final static int NUM_CLASSES = Language.values().length;
    
    private final static String NON_ALPHA = "[^a-zA-Z \' à è é î í ì ó ò ú ù]";
    private final static String VOWELS = "aeiouàèéîíìóòúù";
    private final static String ACCENT_CHARS = "àèéîíìóòúù";
    private final static double SCALING_FACTOR = 100.0;
    
    private double[] input;
    private Language lang;

    /**
     * Construct a new LanguageExample.
     * @param input     The input of this example.
     * @param lang      The language of this example.
     */
    public LanguageExample(double[] input, Language lang) {
        this.input = input;
        this.lang = lang;
    }

    /**
     * Get this example's input array.
     * @return This example's input array.
     */
    public double[] getInput() {
        return input;
    }

    /**
     * Get this example's output array.
     * @return This example's output array.
     */
    public double[] getOutput() {
        return lang.toOutput();
    }

    public Language getLanguage() {
        return lang;
    }

    /**
     * Get a String representation of this LanguageExample.
     * @return a String representation of this LanguageExample.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        for (int i = 0; i < input.length; i++) {
            sb.append(input[i]);
            if (i != input.length - 1) sb.append(", ");
        }
        sb.append("}, " + lang);
        return sb.toString();
    }

    /**
     * Given a String input, parse it according to the functionalization rules
     * for LanguageExamples and return a double array of values representing
     * that String (to be used as an input value to a NeuralNetwork).
     * @paran lString   A language String.
     */
    public static double[] parseLanguageString(String lString) 
        throws IllegalArgumentException {

        if (lString.length() == 0)
            throw new IllegalArgumentException("0-length input String.");
        
        lString = cleanLanguageString(lString);        
        String[] tokens = parseWords(lString);

        double[] input = new double[INPUT_DIM];

        input[0] = averageWordLength(tokens) / SCALING_FACTOR;
        input[1] = endsWithVowel(tokens);
        input[2] = wordsWithDoubleOs(tokens);
        input[3] = (double) longestWord(tokens) / SCALING_FACTOR;
        input[4] = wordsWithDoubleLetters(tokens);
        input[5] = percentWithApostrophes(tokens);
        input[6] = percentKs(lString);
        input[7] = percentAccentChars(lString);
        input[8] = wordsWithIJ(tokens);
        
        return input;
    }

    /**
     * Return a cleaned language String (no numbers, punctuation, etc.) that is
     * all lower-case letters.
     * @param   lString the String to be cleaned
     * @return  The input String, free of non-pertinent information.
     */
    private static String cleanLanguageString(String lString) {
        lString = lString.toLowerCase();
        return lString.replaceAll(NON_ALPHA, "");
    }

    /**
     * Parse the words in a String out into an array.
     * @param lString   The String to parse out.
     * @return  An array of all words in the input String.
     */
    private static String[] parseWords(String lString) {
        return lString.split("\\s+");
    }

    /**
     * Parse a file for containing examples and return a list of LanguageExample
     * instances.
     * @param path  The path to the input file
     * @param lang  The language of the examples in that file.
     * @return      An ArrayList of LanguageExample instances that are parsed
     *              from the file.
     * Precondition: The file should contain only examples of the specified
     *               language, and each example should be followed by the 
     *               separator sentinel string (LanguageExample.SEPARATOR) on a
     *               line by itself.
     */
    public static ArrayList<LanguageExample> 
    getExamples(String path, Language lang)
    throws FileNotFoundException, UnsupportedEncodingException {
        
        //a whole bunch of decorator rigamarole to read UTF streams
        //on the cs.rit machines 
        FileInputStream fStream = new FileInputStream(path);
        InputStreamReader utfReader = new InputStreamReader(fStream, "UTF-8");
        BufferedReader buffer = new BufferedReader(utfReader);
        Scanner scanner = new Scanner(buffer);

        ArrayList<LanguageExample> examples = new ArrayList<LanguageExample>();
        StringBuilder sb = new StringBuilder();
        String line;
        double[] input;
        
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            if (!line.equals(SEPARATOR)) {
                sb.append(line);
            }
            else {
                input = parseLanguageString(sb.toString());
                examples.add(new LanguageExample(input, lang));
                sb = new StringBuilder();
            }
        }
        return examples;
    }

    /**
     * Given an array of words, return the percentage of those words that
     * contain the sequence "ij".
     * @return  The percentage of words in the input array that contain the
     *          sequence "ij".
     */
    private static double wordsWithIJ(String[] tokens) {
        String ij = "ij";
        int count = 0;

        for (int i = 0; i < tokens.length; i++) {
            if (tokens[i].contains(ij)) count++;
        }
        return (double) count / (double) tokens.length;
    }

    /**
     * Given an array of words, return the percentage of those words that
     * contain double letters.
     * @return The percentage of words in the input array that contain
     *         double letters.
     */
    private static double wordsWithDoubleLetters(String[] tokens) {

        int count = 0;

        for (int i = 0; i < tokens.length; i++) {
            for (int j = 0; j < tokens[i].length() - 1; j++) {
                if (tokens[i].charAt(j) == tokens[i].charAt(j+1))
                    count++;
            }
        }
        return (double) count / (double) tokens.length;
    }

    /**
     * Given a String, return the percentage of characters that are "accent"
     * characters.
     * @return The percentage of input characters that are "accent" characters.
     */
    private static double percentAccentChars(String lString) {

        int count = 0;

        for (int i = 0; i < lString.length(); i++) {
            if (ACCENT_CHARS.indexOf(lString.charAt(i)) != -1) count++;
        }
        return (double) count / (double) lString.length();
    }

    /**
     * Given an array of words, return the average word length.
     * @return the average length of the words in the array.
     */
    private static double averageWordLength(String[] tokens) {

        double average = 0;

        for (int i = 0; i < tokens.length; i++)
            average += tokens[i].length();

        average = (average / (double) tokens.length);
        return average;
    }

    /**
     * Given an array of words, return the percentage of those words that
     * end in a vowel (including accented vowels).
     * @return The percentage of input words that end in a vowel.
     */
    private static double endsWithVowel(String[] tokens) {
        
        int count = 0;

        for (int i = 0; i < tokens.length; i++) {
            if ((tokens[i].length() > 1) &&
               (VOWELS.indexOf(tokens[i].charAt(tokens[i].length() - 1)) != -1)) 
                {
                    count++;
            }
        }
        return (double) count / (double) tokens.length;
    }

    /**
     * Given a String, return the percentage of characters in that String
     * that are the letter 'k'.
     * @return the percentage of 'k' characters in the input String.
     */
    private static double percentKs(String lString) {
        int count = 0;

        for (int i = 0; i < lString.length(); i++) {
            if (lString.charAt(i) == 'k') {
                count++;
            }
        }
        return (double) count / (double) lString.length();
    }

    /**
     * Given an array of words, return the percentage of words that contain
     * the sequence of characters 'oo'.
     * @return The percentage of words that contain double o's.
     */
    private static double wordsWithDoubleOs(String[] tokens) {
        String oo = "oo";
        int count = 0;

        for (int i = 0; i < tokens.length; i++) {
            if (tokens[i].contains(oo)) count++;
        }
        return (double) count / (double) tokens.length;
    }

    /**
     * Given an array of words, return the percentage of words that
     * contain an apostrophe.
     * @return the percentage of words that contain an apostrophe.
     */
    private static double percentWithApostrophes(String[] tokens) {
        String apostrophe = "\'";
        int count = 0;

        for (int i = 0; i < tokens.length; i++) {
            if (tokens[i].contains(apostrophe)) {
                count++;
            }
        }
        return (double) count / (double) tokens.length;
    }

    /**
     * Given an array of words, return the length of the longest word.
     * @return The length of the longest word in the input array.
     */
    private static int longestWord(String[] tokens) {
        
        int longestWord = 0;

        for (int i = 0; i < tokens.length; i++) {
            if (tokens[i].length() > longestWord) {
                longestWord = tokens[i].length();
            }
        }
        return longestWord;
    }
}
