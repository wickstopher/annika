import java.util.Random;
import java.util.Arrays;
import java.io.Serializable;


/** 
 * Implements a general-purpose three-layer neural network classifier.
 * @author Christopher R. Wicks (cw9887@cs.rit.edu)
 */
public class NeuralNetwork implements Serializable {

    private double[][] inputWeights;    //weights from inputs to hidden layer
    private double[][] hiddenWeights;   //weights from hidden layer to output

    //Input dimension, hidden layer size, number of output classes
    private int nInputs, hSize, nClasses;

    private static final double ALPHA = 0.1;    //Learning rate
    private static final double BIAS = 1.0;     //Value of bias input

    /**
     * Instantiate a new NeuralNetwork instance.
     * @param nInputs   The number of input dimensions
     * @param hSize     The number of perceptrons in the hidden layer
     * @param nClasses  Number of output classes
     */
    public NeuralNetwork(int nInputs, int hSize, int nClasses) {
        this.nInputs = nInputs;
        this.hSize = hSize ;
        this.nClasses = nClasses;
        inputWeights = new double[nInputs+1][hSize];
        hiddenWeights = new double[hSize+1][nClasses];
    }

    /**
     * Learn an example through backpropagation.
     * @param e     An ANNExample that is to be learned.
     * @return      The error of the given example (prior to backpropagation)
     */
    public double backpropagate(ANNExample e) {

        double[] input = e.getInput();              //input values
        double[] actual = e.getOutput();            //actual example output

        double[][] predictResults = predict(input); //make a prediction

        double[] guess = predictResults[0];      //prediction (output)
        double[] hiddenVals = predictResults[1]; //pre-sig hidden layer inputs
        double[] outputVals = predictResults[2]; //pre-sig output layer inputs 

        double error_i, error_j, delta_i, delta_j;
        double total_error = 0;
        
        for (int i = 0; i < nClasses; i++) {
            
            error_i = actual[i] - guess[i];
            total_error += error_i * error_i;
            delta_i = error_i * sigmoidPrime(outputVals[i]);
            error_j = 0;

            for (int j = 0; j < hSize; j++) {
                hiddenWeights[j][i] += 
                    (ALPHA * delta_i * sigmoid(hiddenVals[j]));
                error_j += delta_i * hiddenWeights[j][i];
            }
            hiddenWeights[hSize][i] += ALPHA * delta_i * BIAS; //apply bias
            error_j += delta_i * BIAS;

            for (int j = 0; j < hSize; j++) {
                delta_j = error_j * sigmoidPrime(hiddenVals[j]);
                for (int k = 0; k < nInputs; k++) {
                    inputWeights[k][j] += ALPHA * delta_j * input[k];
                }
                inputWeights[nInputs][j] += ALPHA * delta_j * BIAS; //apply bias
            }
        }
        return total_error;
    }

    /**
     * Given some input, return this NeuralNetwork's output prediction.
     * @param input     Some input values.
     * @return          This network's output given that input.
     */
    public double[] getPrediction(double[] input) {
        return predict(input)[0];
    }

    /**
     * Initialize this network's weights with random values.
     */
    public void setWeights() {
        initWeights(new Random());
    }

    /**
     * Initialize this network's weights with random values on a given seed.
     * @param seed  The seed to provide to the pseudorandom number generator.
     */
    public void setWeights(long seed) {
        initWeights(new Random(seed));
    }

    /**
     * Make a prediction on some input.
     * @param input     Some input values.
     * @return  An array of double arrays.
     *          index 0 contains the output prediction
     *          index 1 contains hidden-layer inputs
     *          index 2 contains output-layer inputs
     */
    private double[][] predict(double[] input) {
        
        //store intermediary values at hidden layer (pre-sigmoid application)
        double[] hiddenVals = new double[hSize];
        Arrays.fill(hiddenVals, 0.0);

        //store intermediary values at output layer (pre-sigmoid application)
        double[] outputVals = new double[nClasses];
        Arrays.fill(outputVals, 0.0);

        //store the prediction
        double[] guess = new double[nClasses];
        Arrays.fill(guess, 0.0);

        //calculate inputs into the hidden layer
        for (int i = 0; i < nInputs; i++) {
            for (int j = 0; j < hSize; j++) {
                hiddenVals[j] += input[i] * inputWeights[i][j];
            }
        }
        //add bias input
        for (int j = 0; j < hSize; j++) {
            hiddenVals[j] += BIAS * inputWeights[nInputs][j];
        }

        //calculate inputs into output layer
        for (int i = 0; i < hSize; i++) {
            for (int j = 0; j < nClasses; j++) {
                outputVals[j] += (sigmoid(hiddenVals[i]) * hiddenWeights[i][j]);
            }
        }
        for (int j = 0; j < nClasses; j++) {
            outputVals[j] += BIAS * hiddenWeights[hSize][j];
        }

        //calculate output
        for (int i = 0; i < nClasses; i++) {
            guess[i] = sigmoid(outputVals[i]);
        }

        double[][] toReturn = new double[3][];
        toReturn[0] = guess;
        toReturn[1] = hiddenVals;
        toReturn[2] = outputVals;
        return toReturn;
    }

    /**
     * Initialize weights given a Random instance.
     * @param prng  A Random instance.
     */
    private void initWeights(Random prng) {
        for (int i = 0; i < nInputs + 1; i++) {
            for (int j = 0; j < hSize; j++) {
                inputWeights[i][j] = (prng.nextDouble() * 2.0) - 1.0;
            }
        }
        for (int i = 0; i < hSize + 1; i++) {
            for (int j = 0; j < nClasses; j++) {
                hiddenWeights[i][j] = (prng.nextDouble() * 2.0) - 1.0;
            }
        }
    }

    /**
     * Apply the sigmoid function to the input value.
     * @param  x    Input value.
     * @return      sigmoid of x
     */
    private double sigmoid(double x) {
        return 1.0 / (1.0 + Math.pow(Math.E, -x));
    }

    /**
     * Apply the derivative of the sigmoid function to the input value.
     * @param x     Input value
     * @return      sigmoid prime of x
     */
    private double sigmoidPrime(double x) {
        double sig = sigmoid(x);
        return sig * (1.0 - sig);
    }

    /**
     * Return a String representation of this NeuralNetwork isntance.
     * @return a String representation of this NeuralNetwork.
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Number of inputs: " + nInputs + "\n");
        sb.append("Hidden Layer Size: " + hSize + "\n");
        sb.append("Number of output classes: " + nClasses + "\n");

        sb.append("Input weights to hidden layer\n");
        sb.append("=============================\n");

        for (int i = 0; i < inputWeights.length; i++) {
            for (int j = 0; j < inputWeights[0].length; j++) {
                sb.append(" " + inputWeights[i][j] + ",");
            }
            sb.append("\n");
        }

        sb.append("Hidden layer weights to output layer\n");

        for (int i = 0; i < hiddenWeights.length; i++) {
            for (int j = 0; j < hiddenWeights[0].length; j++) {
                sb.append(" " + hiddenWeights[i][j] + ", ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}