import java.util.Arrays;

/**
 * An enum set of Languages; for use by the LanguageExample class of 
 * ANNExamples. 
 * @author Christopher R. Wicks (cw9887@cs.rit.edu)
 */
public enum Language { 
    
    ENGLISH, 
    ITALIAN, 
    DUTCH;

    /**
     * Convert a Language instance to output for a LanguageExample.
     * @return  An array that can be used as the output of a LanguageExample.
     */
    public double[] toOutput() {
        double[] output = new double[3];
        Arrays.fill(output, 0.0);
        switch(this) {
            case ENGLISH:   output[0] = 1.0;
                            break;
            case ITALIAN:   output[1] = 1.0;
                            break;
            case DUTCH:     output[2] = 1.0;
                            break;
        }
        return output;
    }

    /**
     * Given the output of a NeuralNetwork that has been trained on
     * LanguageExamples, return the Language that best represents that output.
     * @param results   The results array given by a NeuralNetwork.
     * @return  The Language that best describes those results.
     */
    public static Language interpretResults(double[] results) {
        if (results[0] > results[1] && results[0] > results[2])
            return ENGLISH;
        if (results[1] > results[2] && results[1] > results[0])
            return ITALIAN;
        return DUTCH;        
    }

    /**
     * Print this Language as a String.
     * @param A String describing this Language.
     */
    public String toString() {
        switch(this) {
            case ENGLISH: return "English";
            case ITALIAN: return "Italian";
            case DUTCH: return "Dutch";
        }
        return new String("Bad language");
    }
}