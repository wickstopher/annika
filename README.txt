=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
| ANNika                                                          |
| An extensible Artificial Neural Network implementation with a   |
| training driver and examples for basic language differentiation |
| (Dutch, English, and Italian).                                  |
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

    * 048277.ann    - Serialized NeuralNetwork instance (the best one)
    * ANNExample.java - Java interface for ANNExamples
    * ANNika.java - driver application
    * examples/
        * dutch.txt - Dutch language examples
        * english.txt - English language examples
        * italian.txt - Italian language examples
    * Language.java - enum class for Language classes
    * LanguageExample.java - Implementation of ANNExample for Languages
    * Makefile - project makefile
    * NeuralNetwork.java - General-purpose ANN implementation
    * wicks_lab2_writeup.pdf - Project writeup


=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
| Compiling and running the application |
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

    Prerequisites: Java jdk 7 or 8, GNU make

    To compile, simply navigate into the directory containing the source code
    and type "make" or "make compile". It will compile the source code with the
    appropriate compiler flags for processing UTF-8 input streams.

    To run the application in query mode with the provided serialized
    NeuralNetwork instance, simply type "make run". It should initiate the
    ANNika application in query mode with the reconstituted NeuralNetwork.

    To summarize, to compile and run the application, simply issue the following
    commands:

        * make
        * make run

    If GNU make is not available on your system, use the following commands to
    compile and run the application:

        * javac -encoding UTF-8 *.java
        * java ANNika 048277.ann


=-=-=-=-=-=-=-=-=-=
| ANNika Commands |
=-=-=-=-=-=-=-=-=-=

    At the query prompt, the user has 4 commands available:

        d                   Enter an arbitrary text segment followed by the
                            "d" command on a new line, and the program will
                            attempt to classify the input segment as either
                            English, Italian or Dutch.

        test                Test this NeuralNetwork instance on all of the
                            avaiable example data and output the number of
                            misclassified examples.

        save [filepath]     Serialize the current NeuralNetwork instance to the
                            filesystem at [filepath].

        q                   Exit the application.


=-=-=-=-=-=-=-=-=-=-=-=-=
| Command-line options  |
=-=-=-=-=-=-=-=-=-=-=-=-=

    The ANNika application can be run in 4 different modes depending on the 
    number of arguments provided at the command-line:

        If only 1 argument, that argument is the filepath to a serialized 
        NeuralNetwork object.
        
        Otherwise:
        
            first argument  = size of hidden layer (int)
            second argument = percentage of data to use as training set
            third argument  = seed for pseudorandom number generator
            fourth argument = number of epochs to train
        
        If 1 argument is present, the program will attempt to read a serialized
        NeuralNetwork instance from the specified filepath, and will then 
        initiate a prompt at which the network can be queried.
        
        If 2 arguments are present, the program will train indefinitely on a
        randomized seed value, and when interrupted, append output to a file
        called output.txt.
        
        If 3 arguments are present, the program will train indefinitely on the
        provided seed value, and when interrupted, append output to a file
        called output.txt.
         
        If 4 arguments are present, the program will train on the provided seed
        value for the specified number of epochs, and will then initiate
        a prompt at which the network can be queried for its interpretation of
        arbitrary input. It will no longer learn, it will simply accept input
        and generate output. The user can also choose to serialize the
        NeuralNetwork instance to the filesystem from the prompt if so desired.
